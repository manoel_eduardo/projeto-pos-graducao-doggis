import os

from flask import Flask, render_template
from flask_mysqldb import MySQL
from flask_cors import CORS
from flask import request
from waitress import serve

from controller.clientes_controller import ClientesController
from controller.relatorio_servico_funcionario import RelatorioServicoFuncionarioController

app = Flask(__name__)
CORS(app)

if 'flask_prod' in os.environ and 'mysql_host' in os.environ:
    app.config['MYSQL_HOST'] = str(os.environ['mysql_host'])
else:
    app.config['MYSQL_HOST'] = 'localhost'

app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '123456'
app.config['MYSQL_DB'] = 'doggis'

mysql = MySQL(app)

@app.route('/api')
def index():
    print(app.config['MYSQL_HOST'])
    return "Hello World!"

@app.route('/api/cliente', methods=['GET'])
def get_cliente():
    return ClientesController(mysql=mysql).list()

@app.route('/api/cliente', methods=['POST'])
def post_cliente():
    try:
        return ClientesController(mysql=mysql).create(request.json)       
    except:
        return {"error":"Não foi possível criar cliente."}, 500
        
@app.route('/api/relatorio-servico-funcionario/<data>', methods=['GET'])
def get_relatorio_servico_funcionario(data: str):
    try:
        return RelatorioServicoFuncionarioController(mysql=mysql).get(data)
    except:
        return {"error":"Não foi recuperar dados para o relatório."}, 500


if __name__ == '__main__':
    if 'flask_prod' in os.environ:
        serve(app, host='0.0.0.0', port=80)
    else:
        app.run(debug=True)