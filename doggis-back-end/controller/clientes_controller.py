import datetime

class ClientesController:
    def __init__(self, mysql):
        self.mysql = mysql
        self.cursor = mysql.connection.cursor()
        self.required_create_fields = ["nome", "cgc"]
        self.insert_model_fields = ["nome", "cgc", "identidade", "telefone", "email", "endereco"]
        self.select_model_fields = ["id", "nome", "cgc", "identidade", "telefone", "email", "endereco", "criado_em"]

    def list(self):
        select_fields = ""
        for field in self.select_model_fields:
            select_fields += ", " if len(select_fields) > 0 else ""
            select_fields += field

        query = f"""
            SELECT 
                {select_fields}
            FROM clientes;
        """
        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchall()
        except:
            return {"error":"Erro ao persistir dados."}, 500

        result = []
        for row in rows:
            obj = {}
            for i in range(0, len(self.select_model_fields)):
                field = self.select_model_fields[i]
                if type(row[i]) is datetime.datetime:
                    obj.update({field: row[i].strftime("%d/%m/%Y %H:%M")}) # TODO
                else:
                    obj.update({field: row[i]})
            result.append(obj)
        return {"success": result}, 200

    def create(self, payload):
        # Validar campos obrigatórios
        for field in self.required_create_fields:
            if field not in payload or payload[field] is None or len(payload[field]) == 0:
                return {"error": f"Campo obrigatório '{field.capitalize()}' não foi preenchido."}, 500
        
        # Validar cgc
        if len(self.get_by_cgc(payload["cgc"])) > 0:
            return {"error": "CPF já registrado no banco de dados."}, 500

        # Criar query de insert
        insert_fields = ""
        insert_values = ""
        for field in self.insert_model_fields:
            if field in payload:
                insert_fields += ", " if len(insert_fields) > 0 else ""
                insert_fields += field
        
                insert_values += "', '" if len(insert_values) > 0 else "'"
                insert_values += payload[field]
        
        insert_values += "'" if len(insert_values) > 0 else ""
        
        query = f"INSERT INTO clientes({insert_fields}) VALUES ({insert_values});"
        
        try:
            self.cursor.execute(query)
            self.mysql.connection.commit()
            return {"success": "Cliente criado com sucesso."}, 200
        except:
            return {"error":"Erro ao persistir dados."}, 500

    def get_by_cgc(self, cgc: str):
        query = f"SELECT * FROM clientes WHERE cgc = '{cgc}';"
        self.cursor.execute(query)
        return self.cursor.fetchall()