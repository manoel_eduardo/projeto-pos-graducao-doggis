import datetime

class RelatorioServicoFuncionarioController:
    def __init__(self, mysql):
        self.mysql = mysql
        self.cursor = mysql.connection.cursor()

    def serialize(self, select_model_fields: list, rows: list):
        result = []
        for row in rows:
            obj = {}
            for i in range(0, len(select_model_fields)):
                field = select_model_fields[i]
                if type(row[i]) is datetime.datetime:
                    obj.update({field: row[i].strftime("%d/%m/%Y %H:%M")}) # TODO
                else:
                    obj.update({field: row[i]})
            result.append(obj)
        return result

    def get_data_final(self, data_inicial: str):
        query = f"SELECT LAST_DAY('{data_inicial}');"
        self.cursor.execute(query)
        return self.cursor.fetchone()[0].strftime("%Y-%m-%d")

    def get_veterinarios(self):
        select_model_fields = ["id", "nome"]
        select_fields = ""
        for field in select_model_fields:
            select_fields += ", " if len(select_fields) > 0 else ""
            select_fields += field

        query = f"""
            SELECT 
                {select_fields}
            FROM veterinario;
        """
        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchall()
            return self.serialize(select_model_fields=select_model_fields, rows=rows)
        except:
            return {"error":"Erro ao selecionar dados."}, 500

    def get_servicos_veterinario(self, veterinario_id: int, data_inicial: str, data_final: str):
        query = f"""
        SELECT
            se.nome as 'servico',
            cl.nome as 'cliente',
            pe.nome as 'pet',
            es.agendado_para
        FROM
            extrato_servico as es 
            INNER JOIN servico se ON es.`idServico` = se.id
            INNER JOIN pet pe ON es.`idPet` = pe.id
            INNER JOIN clientes cl ON pe.`idCliente` = cl.id
        WHERE
            es.`idVeterinario` = {veterinario_id}
        AND es.ausencia = 0
        AND es.agendado_para BETWEEN '{data_inicial} 00:00:00' AND '{data_final} 23:59:59';
        """

        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchall()
            return self.serialize(select_model_fields=["servico", "cliente", "pet", "agendado_para"], rows=rows)
        except:
            return {"error":"Erro ao selecionar dados."}, 500

    def get_atendentes(self):
        select_model_fields = ["id", "nome"]
        select_fields = ""
        for field in select_model_fields:
            select_fields += ", " if len(select_fields) > 0 else ""
            select_fields += field

        query = f"""
            SELECT 
                {select_fields}
            FROM atendente;
        """
        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchall()
            return self.serialize(select_model_fields=select_model_fields, rows=rows)
        except:
            return {"error":"Erro ao selecionar dados."}, 500

    def get_servicos_atendente(self, atendente_id: int, data_inicial: str, data_final: str):
        query = f"""
        SELECT
            se.nome as 'servico',
            cl.nome as 'cliente',
            pe.nome as 'pet',
            es.agendado_para
        FROM
            extrato_servico as es 
            INNER JOIN servico se ON es.`idServico` = se.id
            INNER JOIN pet pe ON es.`idPet` = pe.id
            INNER JOIN clientes cl ON pe.`idCliente` = cl.id
        WHERE
            es.`idAtendente` = {atendente_id}
        AND es.ausencia = 0
        AND es.agendado_para BETWEEN '{data_inicial} 00:00:00' AND '{data_final} 23:59:59';
        """

        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchall()
            return self.serialize(select_model_fields=["servico", "cliente", "pet", "agendado_para"], rows=rows)
        except:
            return {"error":"Erro ao selecionar dados."}, 500

    def get(self, data: str):
        data_inicial = f"{data[2:6]}-{data[0:2]}-01"
        data_final = self.get_data_final(data_inicial)

        veterinarios = self.get_veterinarios()
        atendentes = self.get_atendentes()

        result = {"veterinarios": [], "atendentes": []}

        for veterinario in veterinarios:
            servicos = self.get_servicos_veterinario(
                veterinario_id=veterinario["id"], 
                data_inicial=data_inicial,
                data_final=data_final
            )        

            result["veterinarios"].append({
                "id": veterinario["id"],
                "nome": veterinario["nome"],
                "servicos": servicos
            })

        for atendente in atendentes:
            servicos = self.get_servicos_atendente(
                atendente_id=atendente["id"], 
                data_inicial=data_inicial,
                data_final=data_final
            )        

            result["atendentes"].append({
                "id": veterinario["id"],
                "nome": veterinario["nome"],
                "servicos": servicos
            })
        
        return {"success": result}, 200