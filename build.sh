#!/bin/bash
echo "Building front-end..."
cd doggis-front-end
ng build --prod

echo "Starting containers..."
cd ..
docker-compose up