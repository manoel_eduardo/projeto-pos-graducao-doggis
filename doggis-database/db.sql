DROP DATABASE IF EXISTS doggis;

CREATE DATABASE IF NOT EXISTS doggis;

CREATE TABLE `doggis`.`clientes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(1024) NULL,
  `cgc` VARCHAR(14) NULL,
  `identidade` VARCHAR(10) NULL,
  `telefone` VARCHAR(11) NULL,
  `email` VARCHAR(1024) NULL,
  `endereco` VARCHAR(2048) NULL,
  `criado_em` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

INSERT INTO doggis.clientes (id, nome,cgc,identidade,telefone,email,endereco) VALUES  
(1, 'Maria','00011122233','0123456789','3133334444','maria@teste.com','Rua Teste da Maria, 123'),
(2, 'João','00044455566','9876543210','3032105555','joao@teste.com','Ruta Teste do João, 321');

CREATE TABLE `doggis`.`atendente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(1024) NULL,
  `cgc` VARCHAR(14) NULL,
  `identidade` VARCHAR(10) NULL,
  `telefone` VARCHAR(11) NULL,
  `email` VARCHAR(1024) NULL,
  `endereco` VARCHAR(2048) NULL,
  `usuario` VARCHAR(1024) NULL,
  `senha` VARCHAR(1024) NULL,
  `criado_em` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

INSERT INTO doggis.atendente (id, nome,cgc,identidade,telefone,email,endereco,usuario,senha) VALUES 
(1, 'Fernando','55577788899','6549872310','21555544444','fernando_atendente@teste.com','Rua Testo do Fernando, 354','atendente_fernando','132456');

CREATE TABLE `doggis`.`veterinario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(1024) NULL,
  `cgc` VARCHAR(14) NULL,
  `identidade` VARCHAR(10) NULL,
  `telefone` VARCHAR(11) NULL,
  `email` VARCHAR(1024) NULL,
  `endereco` VARCHAR(2048) NULL,
  `especialidades_especies` VARCHAR(1024) NULL,
  `registro_conselho` VARCHAR(1024) NULL,
  `criado_em` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

INSERT INTO doggis.veterinario (id,nome,cgc,identidade,telefone,email,endereco,especialidades_especies,registro_conselho) VALUES 
(1,'Patricia','99988877766','3692581470','1577775555','patricia_veterinaria@teste.com','Rua Teste da Patricia, 789','pássaro|gato','1231432')
,(2,'Roberto','65432178945','8765132','4555551234','roberto_veterinario@teste.com','Rua do Roberto','cachorro','78531');

CREATE TABLE `doggis`.`pet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT NOT NULL,
  `nome` VARCHAR(1024) NOT NULL,
  `especie` ENUM('cachorro', 'gato', 'pássaro', 'coelho') NOT NULL,
  `raca` VARCHAR(1024) NULL,
  `porte` ENUM('pequeno', 'medio', 'grande') NULL,
  `criado_em` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `pet_idCliente_fk_idx` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `pet_idCliente_fk`
    FOREIGN KEY (`idCliente`)
    REFERENCES `doggis`.`clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO doggis.pet (id, `idCliente`,nome,especie,raca,porte) VALUES 
(1,1,'Mariah Carey ','pássaro','Papagaio',NULL)
,(2,1,'Thor','cachorro','Chihuahua','pequeno')
,(3,2,'Flor','gato','Sphynx','medio');

CREATE TABLE `doggis`.`extrato` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `criado_em` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

INSERT INTO doggis.extrato (id, criado_em) VALUES
(1, '2019-08-01 12:00:00'), (2, '2019-08-02 11:30:00'), (3, '2019-08-03 13:00:00');

CREATE TABLE `doggis`.`servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(1024) NOT NULL,
  `descricao` VARCHAR(5012) NULL,
  `valor` DECIMAL(10,2) NULL,
  `tempo` DECIMAL(10,2) NULL,
  `credito_pataz` DECIMAL(10,2) NULL,
  `valor_pataz` DECIMAL(10,2) NULL,
  `criado_em` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

INSERT INTO doggis.servico (id, nome, descricao, valor, tempo, credito_pataz, valor_pataz) VALUES
(1, 'Tosa', 'Tirar o pelo, plumas, pemas e escamas do pet.', 30.15, 1.00, 12.37, 500.00)
,(2, 'Banho', 'Limpar pelo e pelo do pet.', 30.85, 0.50, 5.93, 450.00)
,(3, 'Vacina', 'Tratamento preventivo contra doenças.', 25.50, 0.25, 10.07, 600.00)
,(4, 'Atendimento veterinário', 'Atendimento clínico para observação do estado de saúde do pet.', 165.28, 1.50, 30.54, 1000.00);

CREATE TABLE `doggis`.`extrato_servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idExtrato` INT NOT NULL,
  `idServico` INT NOT NULL,
  `idPet` INT NULL,
  `idVeterinario` INT NULL,
  `idAtendente` INT NULL,
  `agendado_para` DATETIME NULL,
  `ausencia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `extrato_servico_idExtrato_fk_idx` (`idExtrato` ASC) VISIBLE,
  INDEX `extrato_servico_idServico_fk_idx` (`idServico` ASC) VISIBLE,
  INDEX `extrato_servico_idPet_fk_idx` (`idPet` ASC) VISIBLE,
  INDEX `extrato_servico_idVeterinario_fk_idx` (`idVeterinario` ASC) VISIBLE,
  INDEX `extrato_servico_idAtendente_fk_idx` (`idAtendente` ASC) VISIBLE,
  CONSTRAINT `extrato_servico_idExtrato_fk`
    FOREIGN KEY (`idExtrato`)
    REFERENCES `doggis`.`extrato` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `extrato_servico_idServico_fk`
    FOREIGN KEY (`idServico`)
    REFERENCES `doggis`.`servico` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `extrato_servico_idPet_fk`
    FOREIGN KEY (`idPet`)
    REFERENCES `doggis`.`pet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `extrato_servico_idVeterinario_fk`
    FOREIGN KEY (`idVeterinario`)
    REFERENCES `doggis`.`veterinario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `extrato_servico_idAtendente_fk`
    FOREIGN KEY (`idAtendente`)
    REFERENCES `doggis`.`atendente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO doggis.extrato_servico (id, `idExtrato`, `idServico`, `idPet`, `idVeterinario`, `idAtendente`, agendado_para, ausencia) VALUES
(1, 1, 3, 1, 1, NULL, '2019-10-01 12:00:00', 0)
,(2, 2, 1, 3, NULL, 1, '2019-10-02 12:00:00', 0)
,(3, 2, 2, 3, NULL, 1, '2019-10-02 12:00:00', 0)
,(4, 3, 4, 2, 2, NULL, '2019-10-03 13:15:00', 0);
