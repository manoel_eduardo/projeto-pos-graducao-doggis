# Projeto da Pós-Gradução - Doggis

## Autor: Eduardo Manoel de Paula Júnior

### Descrição

Este projeto tem como objetivo implementar os 3 casos de usos selecionados para o desenvolvimento do trabalho final da pós-gradução em Engenharia de Softawre na PUC Minas.

Neste projeto se encontra a implementação dos casos de uso:
- RF - Cadastro de Cliente
- RF - Listagem de Cliente
- RF - Relatório: serviço x funcionário - mensal

### Tecnologias

| Front-end | Back-end  |
| --------- | --------- |
|Angular 8.2| Python 3.6|
