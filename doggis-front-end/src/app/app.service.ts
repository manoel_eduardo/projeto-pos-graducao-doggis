import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from './models/cliente';
import { environment } from '../environments/environment';

@Injectable({providedIn: 'root'})
export class AppService{
  env = environment;

  constructor(private http: HttpClient) {}

  getClienteList(){
    return this.http.get(this.env.apiUrl + '/cliente');
  }

  postCliente(cliente: Cliente){
    return this.http.post(this.env.apiUrl + '/cliente', cliente);
  }

  getRelatorioServicoFuncionario(data: string){
    return this.http.get(this.env.apiUrl + '/relatorio-servico-funcionario/' + data);
  }
}
