import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizacaoClienteComponent } from './atualizacao-cliente.component';

describe('AtualizacaoClienteComponent', () => {
  let component: AtualizacaoClienteComponent;
  let fixture: ComponentFixture<AtualizacaoClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizacaoClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizacaoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
