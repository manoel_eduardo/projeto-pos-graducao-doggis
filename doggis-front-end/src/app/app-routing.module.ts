import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroClienteComponent } from './cadastro-cliente/cadastro-cliente.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ListagemClienteComponent } from './listagem-cliente/listagem-cliente.component';
import { AtualizacaoClienteComponent } from './atualizacao-cliente/atualizacao-cliente.component';
import { RelatorioServicoFuncionarioComponent } from './relatorio-servico-funcionario/relatorio-servico-funcionario.component';


const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'rf-cadastro-cliente', component: CadastroClienteComponent},
  {path: 'rf-listagem-cliente', component: ListagemClienteComponent},
  {path: 'rf-atualizacao-cliente/:id', component: AtualizacaoClienteComponent},
  {path: 'rf-relatorio-servico-funcionario', component: RelatorioServicoFuncionarioComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
