import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Cliente } from '../models/cliente';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listagem-cliente',
  templateUrl: './listagem-cliente.component.html',
  styleUrls: ['./listagem-cliente.component.css']
})
export class ListagemClienteComponent implements OnInit {
  clienteList: Cliente[];

  showError: boolean;
  errorMessage: string;

  constructor(private appService: AppService, private router: Router) { }

  ngOnInit() {
    this.appService.getClienteList().subscribe(
      response => {
          this.showError = false;
          this.clienteList = response['success'] as Cliente[];
      },
      err => {
          this.showError = true;
          this.errorMessage = err.error.error;
      });
  }

  editar(id) {
    this.router.navigate(['/rf-atualizacao-cliente', id]);
  }

}
