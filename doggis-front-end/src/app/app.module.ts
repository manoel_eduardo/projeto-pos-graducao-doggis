import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {NgxMaskModule, IConfig} from 'ngx-mask';
import {DpDatePickerModule} from 'ng2-date-picker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadastroClienteComponent, CadastroClienteSuccessModalContent } from './cadastro-cliente/cadastro-cliente.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ListagemClienteComponent } from './listagem-cliente/listagem-cliente.component';
import { AtualizacaoClienteComponent } from './atualizacao-cliente/atualizacao-cliente.component';
import { RelatorioServicoFuncionarioComponent } from './relatorio-servico-funcionario/relatorio-servico-funcionario.component';

export let ngMaskOptions: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    CadastroClienteComponent,
    HomePageComponent,
    ListagemClienteComponent,
    CadastroClienteSuccessModalContent,
    AtualizacaoClienteComponent,
    RelatorioServicoFuncionarioComponent
  ],
  entryComponents: [CadastroClienteSuccessModalContent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(ngMaskOptions),
    DpDatePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
