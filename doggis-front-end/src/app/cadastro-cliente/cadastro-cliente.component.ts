import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../models/cliente';
import { AppService } from '../app.service';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.css']
})
export class CadastroClienteComponent implements OnInit {
  cliente: Cliente;
  showError: boolean;
  errorMessage: string;
  successMessage: string;

  constructor(private appService: AppService, private modalService: NgbModal, private router: Router) {}

  ngOnInit() {
    this.cliente = new Cliente();
    this.showError = false;
    this.errorMessage = '';
    this.successMessage = '';
  }

  salvar(form) {
    this.appService.postCliente(this.cliente).subscribe(
      response => {
          this.showError = false;
          this.openModal(response['success']);
      },
      err => {
          this.showError = true;
          this.errorMessage = err.error.error;
      });
  }

  openModal(message) {
    const modalRef = this.modalService.open(CadastroClienteSuccessModalContent);
    modalRef.componentInstance.message = message;
    modalRef.result.finally(() =>{
      this.router.navigate(['/rf-listagem-cliente']);
    });
  }
}

@Component({
  selector: 'cadastro-cliente-success-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body text-center">
      <div class="row">
        <div class="col-12">
          <h3>Sucesso!</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          {{message}}
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Fechar</button>
    </div>
  `
})
export class CadastroClienteSuccessModalContent {
  @Input() message;

  constructor(public activeModal: NgbActiveModal) {}
}
