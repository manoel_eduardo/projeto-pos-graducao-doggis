import { Component } from '@angular/core';
import { faSignOutAlt, faDog } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Pet Shop Doggis';
  faSignOutAlt = faSignOutAlt;
  faDog = faDog;
}
