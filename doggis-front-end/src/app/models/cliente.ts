export class Cliente {
  public id: number;
  public nome: string;
  public cgc: string;
  public identidade: string;
  public telefone: string;
  public endereco: string;
  public email: string;
}
