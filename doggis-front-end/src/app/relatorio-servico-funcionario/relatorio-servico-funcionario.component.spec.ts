import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatorioServicoFuncionarioComponent } from './relatorio-servico-funcionario.component';

describe('RelatorioServicoFuncionarioComponent', () => {
  let component: RelatorioServicoFuncionarioComponent;
  let fixture: ComponentFixture<RelatorioServicoFuncionarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatorioServicoFuncionarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatorioServicoFuncionarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
