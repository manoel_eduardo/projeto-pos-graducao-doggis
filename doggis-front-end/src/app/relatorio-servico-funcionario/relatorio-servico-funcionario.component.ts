import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-relatorio-servico-funcionario',
  templateUrl: './relatorio-servico-funcionario.component.html',
  styleUrls: ['./relatorio-servico-funcionario.component.css']
})
export class RelatorioServicoFuncionarioComponent implements OnInit {
  showError: boolean;
  errorMessage: string;
  selectedDate: any;
  relatorio: {};

  constructor(private appService: AppService) {
    this.showError = false;
    this.errorMessage = '';
    this.relatorio = {};
  }

  ngOnInit() {
  }

  gerar() {
    if (this.selectedDate === undefined) {
      this.showError = true;
      this.errorMessage = 'Selecione um mês para poder gerar o relatório';
    } else {
      this.showError = false;
      let data = '';

      try {
        data = this.selectedDate.format('MMYYYY')
      } catch {
        this.showError = true;
        this.errorMessage = 'Erro ao formatar data';
      }

      this.appService.getRelatorioServicoFuncionario(data).subscribe(
        response => {
            this.showError = false;
            this.relatorio = response["success"];
            console.log(this.relatorio);
        },
        err => {
            this.showError = true;
            this.errorMessage = err.error.error;
        });

    }
  }
}
